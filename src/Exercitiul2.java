import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Exercitiul2 {
    public static void main(String[] args) {
        InterfataGrafica ig = new InterfataGrafica();
        SwingUtilities.invokeLater(() -> ig.setVisible(true));
    }
}

class InterfataGrafica extends JFrame {
    JTextField text1 = new JTextField();
    JTextField text2 = new JTextField();
    JButton button = new JButton("Click"); //the text written on the button
    JPanel panel = new JPanel();

    public InterfataGrafica() {
        setTitle("Aplicatie Examen ISP 2021 "); //setting the window title
        setSize(350,250);  //adjusting the size of the window
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        GridLayout gl = new GridLayout(3,1,25,20);
        panel.setLayout(gl);    //add elements to the panel
        add(panel);
        panel.add(text1);   //adding the text1 field to the panel
        panel.add(text2);   //adding the text2 field to the panel
        panel.add(button);  //adding the button to the panel
        //button functionality
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //what happens when I click the button
                charsCounter();
            }
        });
    }
    public void charsCounter() {
        //the number of chars (with spaces)
        text2.setText(String.valueOf(text1.getText().length()));
    }
}

