import java.util.ArrayList;
import java.util.Collection;

public class Exercitiul1 {}

class A {
    public void met(B b) { //through dependency
        //met method implementation
    }
}

class B {
    private long t;
    Collection<D> dCollection; //through aggregation
    E e; //through association
    public void x() {
        //x method implementation
    }
}

interface C {
    //interface body (with unimplemented methods)
}

class D {
    //class D body
}

class E implements C {
    ArrayList<F> fArrayList = new ArrayList<>(); //through composition
    public void metG(int i) {
        //metG method implementation
    }
}

class F {
    public void metA() {
        //metA method implementation
    }
}